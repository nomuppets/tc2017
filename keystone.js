// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config()

// Require keystone
var keystone = require('keystone')

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
  'name': 'TeamCoach',
  'brand': 'TeamCoach',

  'sass': 'public',
  'static': 'public',
  'favicon': 'public/favicon.ico',
  'views': 'templates/views',
  'view engine': 'jade',

  'cloudinary config': 'cloudinary://327821826415529:qA7TUCmYLP0RhR37wftPlustEKo@teamcoach',

  'auto update': true,
  'session': true,
  'session store': 'mongo',
  'auth': true,
  'user model': 'Account'
})

// Leave directly under init
keystone.set('intercom', { appId: 'tgmp9b6q', appApiKey: '0cc93283b2ac82fba39c989d1421546e7f6b1a28' })

// Load your project's Models
keystone.import('models')

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
  _: require('lodash'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
})

// Load your project's Routes
keystone.set('routes', require('./routes'))

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
  accounts: ['accounts', 'avatar-images'],
  cards: ['cards', 'card-types'],
  players: 'players',
  teams: 'teams',
  games: 'silver-codes',
  stats: ['seasons', 'rounds', 'player-season-stats', 'player-round-stats']
})

// Set the admin path to whatever we want
// keystone.set('admin path', 'cms')
keystone.set('file limit', '10mb')

// Start Keystone to connect to your database and initialise the web server
keystone.start()
