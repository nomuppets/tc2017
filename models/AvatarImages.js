'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types

// AvatarImages Model
var AvatarImage = new keystone.List('AvatarImage')

AvatarImage.add({
  name: { type: String, required: true, initial: true },
  player: { type: Types.Relationship, ref: 'Player', required: true, initial: true },
  imageTop: { type: Types.CloudinaryImage, required: true, autoCleanup: true, initial: true },
  imageBottom: { type: Types.CloudinaryImage, required: true, autoCleanup: true, initial: true },
  comments: { type: Types.Html, wysiwyg: false, height: 200 }
})

AvatarImage.defaultColumns = 'name, imageTop, imageBottom'
AvatarImage.register()
