'use strict'

var keystone = require('keystone')
var Types = keystone.Field.Types
var Player = keystone.list('Player')
var PlayerSeasonStat = keystone.list('PlayerSeasonStat')

var request = require('superagent')

// Season Model
var Season = new keystone.List('Season', {
  map: { name: 'year' }
})

var infoFetch = 'Check this checkbox to manually refresh the player data for this season.<br>When you press the Save button the AFL api will be called to repopulate the database with new statistics'

Season.add({
  year: { type: String, required: true, initial: true, label: 'Year', note: 'Season (year)' },
  externalSystemId: { type: String, required: true, initial: true }
}, 'API call', {
  fetchSeasonData: { type: Types.Boolean, initial: true, label: 'Refresh season player statistics', note: infoFetch }
})

Season.defaultColumns = 'year, externalSystemId'
Season.register()

Season.schema.post('save', function (doc) {
  if (doc.fetchSeasonData) {
    let json = ''
    PlayerSeasonStat.model.find({season: doc._id}).remove()
      .exec()
      .then(function () {
        return request.post('http://afl.com.au/api/cfs/afl/WMCTok')
      })
      .then(function (res) {
        const token = res.body.token
        return request.get('http://www.afl.com.au/api/cfs/afl/statsCentre/players?competitionId=' + doc.externalSystemId)
          .set('X-media-mis-token', token)
      })
      .then(function (jsonVar) {
        json = jsonVar.body
        return Player.model.find().exec()
      }).then(function (players) {
        for (let i in players) {
          const externalSystemId = players[i].externalSystemId
          for (let x in json.lists) {
            if (json.lists[x].player.playerId === externalSystemId) {
              /*eslint new-cap: ["error" , { "newIsCap": false }]*/
              var data = new PlayerSeasonStat.model({
                player: players[i]._id,
                season: doc._id,
                totals: json.lists[x].stats.totals,
                averages: json.lists[x].stats.averages
              })
              data.save()
            }
          }
        }
      })
  }
}, function (r) {
  console.log(r.body)
})
