'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types

// Card Model
var Card = new keystone.List('Card', {
  map: { name: 'number' }
})

Card.add({
  number: { type: String, required: true, initial: true },
  cardType: { type: Types.Relationship, ref: 'CardType', required: false, initial: true },
  player: { type: Types.Relationship, ref: 'Player', required: true, initial: true },
  image: { type: Types.CloudinaryImage, required: true, autoCleanup: true, initial: true }
})

Card.defaultColumns = 'number, cardType, player, image'
Card.register()
