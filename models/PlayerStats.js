'use strict'

var stats = {
  kicks: { type: Number, label: 'Kicks' },
  handballs: { type: Number, label: 'Handballs' },
  disposals: { type: Number, label: 'Disposals' },
  marks: { type: Number, label: 'Marks' },
  marksInside50: { type: Number, label: 'Marks inside 50' },
  hitouts: { type: Number, label: 'Hitouts' },
  tackles: { type: Number, label: 'Tackles' },
  goals: { type: Number, label: 'Goals' },
  behinds: { type: Number, label: 'Behinds' }
}

module.exports = stats
