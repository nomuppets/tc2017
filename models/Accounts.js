'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types

var Intercom = require('intercom-client')
var intercomConfig = keystone.get('intercom')
var client = new Intercom.Client({ appId: intercomConfig.appId, appApiKey: intercomConfig.appApiKey })

// User Model
var Account = new keystone.List('Account', {
  track: true
})

var info = 'This screen enables you to manage the users of teamcoach'
var infoAccountType = 'There are 3 types of account: a kid account is created by a guardian, a guardian account is typically a parent, and a normal account is a person of age who is allowed on the teamcoach platform'

const dayOptions = []
const monthOptions = []
const yearOptions = []

const dayNum = 31
for (let x = 1; x <= dayNum; x++) {
  const value = x
  const label = x < 10 ? '0' + x : x
  dayOptions.push({value, label})
}

const monthNum = 12
const monthLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
for (let x = 0; x < monthNum; x++) {
  const value = x
  monthOptions.push({value: value, label: monthLabels[x]})
}

const currentYear = new Date().getFullYear()
const minYear = currentYear - 120
for (let x = currentYear; x > minYear; x--) {
  yearOptions.push(x)
}

Account.add({
  name: { type: Types.Name, required: true, index: true, note: info },
  email: { type: Types.Email, initial: true, required: true, index: true },
  username: { type: String, initial: true, required: true, index: true },
  password: { type: Types.Password, initial: true, required: true },
  dob: {
    day: { type: Types.Select, required: true, initial: true, label: 'Birthday (day)', options: dayOptions, numeric: true },
    month: { type: Types.Select, required: true, initial: true, label: 'Birthday (month)', options: monthOptions, numeric: true },
    year: { type: Types.Select, required: true, initial: true, label: 'Birthday (year)', options: yearOptions, numeric: true },
    utc: { type: Types.Date, noedit: true, label: 'Birthdate' }
  }
}, 'Account Type', {
  accountType: { type: Types.Select, required: true, initial: true, note: infoAccountType, options: 'normal, guardian, kid' }
}, {heading: 'Guardian details', dependsOn: {accountType: 'kid'}}, {
  guardian: { type: Types.Relationship, ref: 'Account', required: false, dependsOn: {accountType: 'kid'} }
}, 'Auditing', {
  isVerified: { type: Types.Boolean, required: false, label: 'Is a verified account', default: false }
}, 'Avatar details', {
  favTeam: { type: Types.Relationship, ref: 'Team', label: 'Favourite team', required: false },
  jumper: { type: String, label: 'Jumper' },
  avatar1: { type: Types.Relationship, ref: 'AvatarImage', label: 'Avatar top image' },
  avatar2: { type: Types.Relationship, ref: 'AvatarImage', label: 'Avatar bottom image' }
}, 'Address', {
  address: { type: Types.Location, defaults: { country: 'Australia' } }
}, 'Permissions', {
  isAdmin: { type: Types.Boolean, label: 'Can access Keystone', index: true }
}, 'Audit Trail', {
  lastSeenIp: { type: String, required: false, label: 'Last seen IP address', noedit: true	}
})

// Provide access to Keystone
Account.schema.virtual('canAccessKeystone').get(function () {
  return this.isAdmin
})

Account.schema.pre('save', function (next, req, callback) {
  // establish if document is new or not
  const dob = this.dob
  this.dob.utc = Date.UTC(dob.year, dob.month, dob.day, 0, 0, 0)
  console.log(this.dob.utc)
  this.wasNew = this.isNew
  next(callback)
})

Account.schema.post('remove', function (doc) {
  client.users.delete({
    user_id: doc.username
  }, function (r) {
    console.log(r.body)
  })
})

// Post save logic
Account.schema.post('save', function (doc) {
  const dob = doc.dob.day + '-' + monthLabels[doc.dob.month] + '-' + doc.dob.year
  client.users.create({
    user_id: doc.username,
    name: doc.name.full,
    email: doc.email,
    signed_up_at: doc.createdAt,
    custom_attributes: {
      isVerified: doc.isVerified,
      birthDate: dob,
      birthDate_at: doc.dob.utc,
      accountType: doc.accountType
    }
  }, function () {
    // console.log(r.body)
  })
})

/**
 * Registration
 */
Account.defaultColumns = 'name, email, accountType, guardian, isAdmin'
Account.register()
