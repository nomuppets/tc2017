'use strict'

var keystone = require('keystone')
var Types = keystone.Field.Types
var Player = keystone.list('Player')
var PlayerRoundStat = keystone.list('PlayerRoundStat')

var request = require('superagent')

// Round Model
var Round = new keystone.List('Round')

var infoFetch = 'Check this checkbox to manually refresh the player data for this round.<br>When you press the Save button the AFL api will be called to repopulate the database with new statistics'

Round.add({
  name: { type: String, required: true, initial: true, note: 'Round Name' },
  sortOrder: { type: Number, required: true, initial: true },
  displayName: { type: String, required: true, initial: true },
  season: { type: Types.Relationship, ref: 'Season', required: true, initial: true, label: 'Season (Year)' },
  externalSystemId: { type: String, required: true, initial: true }
}, 'API call', {
  fetchRoundData: { type: Types.Boolean, initial: true, label: 'Refresh round player statistics', note: infoFetch }
})

Round.defaultColumns = 'name, displayName, season, externalSystemId'
Round.register()

Round.schema.post('save', function (doc) {
  if (doc.fetchRoundData) {
    let json = ''
    PlayerRoundStat.model.find({round: doc._id}).remove()
      .exec()
      .then(function () {
        return request.post('http://afl.com.au/api/cfs/afl/WMCTok')
      })
      .then(function (res) {
        const token = res.body.token
        console.log(token)
        return request.get('http://www.afl.com.au/api/cfs/afl/statsCentre/players?roundId=' + doc.externalSystemId)
          .set('X-media-mis-token', token)
      })
      .then(function (jsonVar) {
        json = jsonVar.body
        return Player.model.find().exec()
      }).then(function (players) {
        for (let i in players) {
          const externalSystemId = players[i].externalSystemId
          for (let x in json.lists) {
            if (json.lists[x].player.playerId === externalSystemId) {
              /*eslint new-cap: ["error" , { "newIsCap": false }]*/
              var data = new PlayerRoundStat.model({
                player: players[i]._id,
                round: doc._id,
                totals: json.lists[x].stats.totals,
                averages: json.lists[x].stats.averages
              })
              data.save()
            }
          }
        }
      }, function (err) {
        console.log(err)
      })
  }
}, function (r) {
  console.log(r.body)
})
