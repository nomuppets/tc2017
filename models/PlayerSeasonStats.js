'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types
var stats = require('./PlayerStats')

// PlayerSeasonStat Model
var PlayerSeasonStat = new keystone.List('PlayerSeasonStat')

PlayerSeasonStat.add({
  player: { type: Types.Relationship, ref: 'Player', required: true, initial: true },
  season: { type: Types.Relationship, ref: 'Season', required: true, initial: true }
}, 'Totals', {
  totals: stats
}, 'Averages', {
  averages: stats
})

PlayerSeasonStat.defaultColumns = 'player, season'
PlayerSeasonStat.register()
