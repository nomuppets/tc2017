'use strict'
// var _ = require('lodash')
var keystone = require('keystone')
var Types = keystone.Field.Types

// Player Model
var Player = new keystone.List('Player', {
  // drilldown: 'team'
})

// var stats = {
//   kicks: { type: Number, label: 'Kicks' },
//   handballs: { type: Number, label: 'Handballs' },
//   disposals: { type: Number, label: 'Disposals' },
//   marks: { type: Number, label: 'Marks' },
//   marksInside50: { type: Number, label: 'Marks inside 50' },
//   hitouts: { type: Number, label: 'Hitouts' },
//   tackles: { type: Number, label: 'Tackles' },
//   goals: { type: Number, label: 'Goals' },
//   behinds: { type: Number, label: 'Behinds' }
// }

// var statsTotals = {
//   totals: {
//     stats
//   }
// }

// var statsAverages = {
//   averages: {
//     stats
//   }
// }

Player.add({
  name: { type: Types.Name, required: true, initial: true },
  externalSystemId: { type: String, required: false, initial: true, unique: true, index: true },
  rank: { type: String, required: true, initial: true },
  jumper: { type: String, required: false, initial: true },
  team: { type: Types.Relationship, ref: 'Team', required: true, initial: true },
  position: { type: Types.Select, required: true, initial: true, options: 'backmen, midfielders, forward, ruckmen' },
  image: { type: Types.CloudinaryImage, required: false, autoCleanup: true },
  comments: { type: Types.Html, wysiwyg: false, height: 200 }
// }, 'Last round statistics (totals)', {
  // lastRoundStatsTotals: _.clone(statsTotals, true)
// }, 'Last round statistics (averages)', {
  // lastRoundStatsAverages: _.clone(statsAverages, true)
// }, 'Current seasons statistics (totals)', {
  // currentSeasonStatsTotals: _.clone(statsTotals, true)
// }, 'Current seasons statistics (averages)', {
  // currentSeasonStatsAverages: _.clone(statsAverages, true)
})

Player.relationship({ ref: 'PlayerSeasonStat', path: 'player-season-stat', refPath: 'player' })

Player.defaultColumns = 'name, image, externalSystemId, team, position'
Player.register()
