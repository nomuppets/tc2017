'use strict'

var keystone = require('keystone')
var Types = keystone.Field.Types

// Team Model
var Team = new keystone.List('Team', {
  track: true
})

Team.add({
  name: { type: String, required: true, initial: true },
  logo: { type: Types.CloudinaryImage, required: true, autoCleanup: true, initial: true },
  shortName: { type: String, required: true, initial: true }
})

Team.relationship({ ref: 'Player', path: 'player', refPath: 'team' })

Team.defaultColumns = 'name, logo, shortName'
Team.register()
