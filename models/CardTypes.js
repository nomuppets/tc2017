'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types

// CardTypes Model
var CardType = new keystone.List('CardType', {
})

CardType.add({
  name: { type: String, required: true, initial: true },
  sortOrder: {type: Number, required: false},
  image: { type: Types.CloudinaryImage, required: true, autoCleanup: true, initial: true }
})

// CardType.relationship({ ref: 'Card', path: 'card', refPath: 'cardType' })

CardType.defaultColumns = 'name, sortOrder, image'
CardType.register()
