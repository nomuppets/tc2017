'use strict'
var keystone = require('keystone')
var Types = keystone.Field.Types
var stats = require('./PlayerStats')

// PlayerRoundStat Model
var PlayerRoundStat = new keystone.List('PlayerRoundStat')

PlayerRoundStat.add({
  player: { type: Types.Relationship, ref: 'Player', required: true, initial: true },
  round: { type: Types.Relationship, ref: 'Round', required: true, initial: true }
}, 'Totals', {
  totals: stats
}, 'Averages', {
  averages: stats
})

PlayerRoundStat.defaultColumns = 'player, round'
PlayerRoundStat.register()
