'use strict'

var keystone = require('keystone')
var Types = keystone.Field.Types

// Silver codes Model
var SilverCode = new keystone.List('SilverCode', {
  map: { name: 'code' }
})

SilverCode.add({
  code: { type: String, required: true, initial: true },
  card: { type: Types.Relationship, ref: 'Card', filters: { cardType: '57d4ff29b3fadbba9f59ca92' }, required: true, initial: true },
  account: { type: Types.Relationship, ref: 'Account', required: false, initial: false }
})

SilverCode.defaultColumns = 'code, card, account'
SilverCode.register()
